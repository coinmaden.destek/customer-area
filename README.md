# WP Customer Area #

WP Customer Area is a modular all-in-one solution to manage private content with WordPress.

## Description ##

WP Customer Area is a modular all-in-one solution to manage private content with WordPress. Sharing files/pages with 
one or multiple users is one of the main feature provided by our easy-to-use plugin. Give it a try!

* [Demos](https://wp-customerarea.com/all-demos)
* [Add-ons](https://wp-customerarea.com/shop/)
* [Documentation](https://wp-customerarea.com/documentation/)
* [Translations](https://wp-customerarea.com/documentation/)
* [Support](https://wp-customerarea.com/support)
* [FAQ](https://wp-customerarea.com/faq/)
* [GitHub repository for contributors](https://github.com/marvinlabs/customer-area)
* [Issue tracker](https://github.com/marvinlabs/customer-area/issues)
